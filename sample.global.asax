﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using Castle.ActiveRecord;
using Castle.ActiveRecord.Framework.Config;

namespace YOURAPP_NAMESPACE {
	public class MvcApplication : System.Web.HttpApplication {
		protected void Application_Start() {
			AreaRegistration.RegisterAllAreas();
			RegisterRoutes(RouteTable.Routes);

			ActiveRecordStarter.Initialize(ActiveRecordSectionHandler.Instance, YOURAPP_ENTITIES_COLLECTION_HELPER.GetTypes());

			Castle.ActiveRecord.ValidatorL10n.TypePrefix = "_";
		}

		protected void Application_BeginRequest(object sender, EventArgs e) {
			HttpContext.Current.Items.Add("ARScope", new SessionScope());
			HttpContext.Current.Items.Add("ARTransScopeStack", new Stack<TransactionScope>());
		}

		protected void Application_EndRequest(object sender, EventArgs e) {
			try {
				if (HttpContext.Current.Items["ARTransScopeStack"] != null) {
					Stack<TransactionScope> transStack = HttpContext.Current.Items["ARTransScopeStack"] as Stack<TransactionScope>;

					while (transStack.Count > 0) {
						TransactionScope trans = transStack.Pop();
						trans.Dispose();
					}
				}

				SessionScope scope = HttpContext.Current.Items["ARScope"] as SessionScope;

				if (scope != null) scope.Dispose();
			} catch (Exception ex) {
				HttpContext.Current.Trace.Warn("Error", "EndRequest: " + ex.Message, ex);
			}
		}
	}
}