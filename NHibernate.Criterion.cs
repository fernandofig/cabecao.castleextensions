﻿using System;
using System.Collections.Generic;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Criterion;
using Castle.ActiveRecord.Framework;
using Castle.ActiveRecord;

namespace NHibernate.Criterion {
	public static class RestrictionsExt {
		public static ICriterion NumberLike(string dbFieldName, string value, MatchMode matchMode) {
			ICriterion r;
			string expr = value.Trim('%');
			string[] likeExpr = new string[] {};
			if (string.IsNullOrWhiteSpace(expr)) return null;

			if (matchMode == MatchMode.Anywhere) {
				likeExpr = new string[] { expr + "%", "%" + expr + "%", "%" + expr, expr };
			} else if (matchMode == MatchMode.Start) {
				likeExpr = new string[] { expr + "%", expr };
			} else if (matchMode == MatchMode.End) {
				likeExpr = new string[] { "%" + expr, expr };
			} else {
				likeExpr = new string[] { expr };
			}

			r = Expression.Sql("{alias}." + dbFieldName + " like ?", likeExpr[0], NHibernate.Type.TypeFactory.GetAnsiStringType(15));

			for (int ix = 1; ix < likeExpr.Length; ix++) {
				r = Expression.Or(r, Expression.Sql("{alias}." + dbFieldName + " like ?", likeExpr[ix], NHibernate.Type.TypeFactory.GetAnsiStringType(15)));
			}

			return r;
		}
	}
}
