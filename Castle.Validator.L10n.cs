﻿using System;
using System.Linq;
using System.Reflection;

namespace Castle.ActiveRecord {
	public class ValidatorL10n {
		public static string GetMessage(string ns, string id, Assembly assemblyDeOrigem) {
			string[] partes = id.Split('.');
			if (partes.Length != 2) return id;

			var q = from t in assemblyDeOrigem.GetTypes()
					where t.IsClass &&
						   t.Name == partes[0] &&
						   t.Namespace == ns &&
						   t.GetProperty(partes[1], BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.IgnoreCase | BindingFlags.DeclaredOnly) != null
					select t;

			if (q.Count() != 1) return id;

			PropertyInfo p = q.FirstOrDefault().GetProperty(partes[1], BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.IgnoreCase | BindingFlags.DeclaredOnly);

			return p.GetValue(p, null).ToString();
		}

		public static string GetTipoLocalizadoIngles(string locNs, Type tp, Assembly assemblyDeOrigem) {
			var q = from t in assemblyDeOrigem.GetTypes()
					where t.IsClass &&
						   t.Name == "Types" &&
						   t.Namespace == locNs &&
						   t.GetProperty(tp.Name, BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.IgnoreCase | BindingFlags.DeclaredOnly) != null
					select t;

			if (q.Count() != 1) return tp.Name;

			PropertyInfo p = q.FirstOrDefault().GetProperty(tp.Name, BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.IgnoreCase | BindingFlags.DeclaredOnly);

			return p.GetValue(p, null).ToString();
		}
	}
}