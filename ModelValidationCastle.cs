﻿using System;
using System.Web.Mvc;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

using Castle.ActiveRecord;

namespace System.Web.Mvc {
	public static class ModelValidationCastleExtensions {
		public static void SetModelErrorState(this Controller ctl, object dbObjAr) {
			Castle.ActiveRecord.Framework.IValidationProvider objar = (Castle.ActiveRecord.Framework.IValidationProvider)dbObjAr;
			IDictionary<System.Reflection.PropertyInfo, ArrayList> obj = (IDictionary<System.Reflection.PropertyInfo, ArrayList>)objar.PropertiesValidationErrorMessages;
			string messageTxt;

			if (obj.Count > 0) {
				foreach (System.Reflection.PropertyInfo umCampo in obj.Keys) {
					string prop = umCampo.Name;

					foreach (string umVal in obj[umCampo] as System.Collections.ArrayList) {
						messageTxt = umVal;
						if (!umVal.Contains(".") && umVal.StartsWith("Field must be") && umVal.EndsWith("characters long")) messageTxt = ValidatorL10n.GetTipoLocalizadoIngles("Resources", dbObjAr.GetType(), Assembly.GetCallingAssembly()) + ".Vld_" + prop + "_TamInv";

						ctl.ModelState.AddModelError(prop, ValidatorL10n.GetMessage("Resources", messageTxt, Assembly.GetCallingAssembly()));
						ctl.ModelState.SetModelValue(prop, ctl.ValueProvider.GetValue(prop));
					}
				}
			}

			ActiveRecordMediator.Evict(dbObjAr);
		}

		public static void SetModelErrorState(this Controller ctl, string campo, string msgerro) {
			ctl.ModelState.AddModelError(campo, msgerro);
			ctl.ModelState.SetModelValue(campo, ctl.ValueProvider.GetValue(campo));
		}

		public static string GetExceptionMessage(this Controller ctl, Exception e) {
			if (e.InnerException != null && e.InnerException.InnerException != null) return e.InnerException.InnerException.Message;
			else if (e.InnerException != null) return e.InnerException.Message;
			else return e.Message;
		}

		public static void AddFrom(this ModelStateDictionary msd, ModelStateDictionary other) {
			foreach (string mk in other.Keys) {
				msd.Add(mk, other[mk]);
			}
		}
	}
}