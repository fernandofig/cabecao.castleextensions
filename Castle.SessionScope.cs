﻿using System;
using System.Collections.Generic;
using NHibernate.Tool.hbm2ddl;
using Castle.ActiveRecord.Framework;
using Castle.ActiveRecord;

namespace Castle.ActiveRecord {
	public static class SessionScopeExtensions {
		public static NHibernate.ISession GetCurrentSession(this ISessionScope ss) {
			return GetCurrentSession();
		}

		public static NHibernate.ISession GetCurrentSession() {
			Castle.ActiveRecord.Framework.ISessionFactoryHolder holder = ActiveRecordMediator.GetSessionFactoryHolder();
			ISessionScope activeScope = holder.ThreadScopeInfo.GetRegisteredScope();
			NHibernate.ISession session = null;
			var key = holder.GetSessionFactory(typeof(ActiveRecordBase));
			if (activeScope == null) {
				session = holder.CreateSession(typeof(ActiveRecordBase));
			} else {
				if (activeScope.IsKeyKnown(key))
					session = activeScope.GetSession(key);
				else
					session = holder.GetSessionFactory(typeof(ActiveRecordBase)).OpenSession();
			}

			return session;
		}
	}
}

namespace Castle.ActiveRecord.Framework {
	public static class SchemaUpdateExtensions {
		public static SchemaExport SchemaExport(this ISessionFactoryHolder sfh, Type type) {
			if (!ActiveRecordStarter.IsInitialized) throw new ActiveRecordException("Framework must be Initialized first.");

			IList<SchemaExport> schex = new List<SchemaExport>();

			NHibernate.Cfg.Configuration cfg = sfh.GetConfiguration(type);

			return new SchemaExport(cfg);
		}

		public static SchemaUpdate SchemaUpdate(this ISessionFactoryHolder sfh, Type type) {
			if (!ActiveRecordStarter.IsInitialized) throw new ActiveRecordException("Framework must be Initialized first.");

			IList<SchemaExport> schex = new List<SchemaExport>();

			NHibernate.Cfg.Configuration cfg = sfh.GetConfiguration(type);

			return new SchemaUpdate(cfg);
		}
	}
}