﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using NHibernate.Linq;
using NHibernate.Linq.Functions;
using NHibernate.Linq.Visitors;
using NHibernate.Dialect;
using NHibernate.Dialect.Function;
using NHibernate.Hql.Ast;
using NHibernate;

namespace Castle.ActiveRecord.Linq {
	public class ExtendedLinqtoHqlGeneratorsRegistry : DefaultLinqToHqlGeneratorsRegistry {
		public ExtendedLinqtoHqlGeneratorsRegistry() {
			this.Merge(new AddDaysGenerator());
		}
	}

	public class AddDaysGenerator : BaseHqlGeneratorForMethod {
		public AddDaysGenerator() {
			SupportedMethods = new[] {
				ReflectionHelper.GetMethodDefinition<DateTime?>(d => d.Value.AddDays((double)0)),
				ReflectionHelper.GetMethodDefinition<DateTimeOffset?>(d => d.Value.AddDays((double)0))
			};
		}

		public override HqlTreeNode BuildHql(MethodInfo method, Expression targetObject, ReadOnlyCollection<Expression> arguments, HqlTreeBuilder treeBuilder, IHqlExpressionVisitor visitor) {
			return treeBuilder.MethodCall("AddDays",
										  visitor.Visit(targetObject).AsExpression(),
										  visitor.Visit(arguments[0]).AsExpression()
										  );
		}
	}

	public class ExtendedSQL2008Dialect : MsSql2008Dialect {
		public ExtendedSQL2008Dialect() {
			RegisterFunction(
				"AddDays",
				new SQLFunctionTemplate(
					NHibernateUtil.DateTime,
					"dateadd(day,?2,?1)"
					)
				);
		}
	}
}